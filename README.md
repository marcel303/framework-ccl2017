# README #

Hi! This is a stable build of the 'framework' creative coding library used by Amy and me during CCL2017, Amsterdam.

In addition to the framework itself, the code available here also includes a project called 'AV Graph'. AV Graph is a node based editor similar to VVVV and TouchDesigner. I'm developing AV Graph as a means to explore the possibilities and ingredients necessary to create a highly creative coding environment.

### Framework; A C++ creative coding library ###

* Framework is a creative coding library being developed in C++.
* It includes support for drawing images and playing sounds with one line of code, facilitating very rapid prototyping during jams or workshops.
* It also includes easy access to keyboard, mouse and up to four gamepads.
* Shader programming is easy using the Shader class.
* All resources (images, sounds, shaders and more) can be tracked in real-time and re-loaded when changed.
* Framework is used for the higher level AV Graph node editor and UI system.

### AV Graph; Node based coding environment ###

* AV Graph is a visual node based editor, adopting the data-flow paradigm to programming.
* AV Graph makes it easy to add new node types by editing the included types.xml and adding the C++ implementation of the node.
* The environment allows one to inspect and preview any input or output socket. Persistent socket value visualizers can be added by right clicking on a socket.
* Graph changes are applied in real-time and previewed behind the editor, providing immediate visual feedback of the work you're doing.

![Scheme](images/avGraph.png)

### How do I get set up? ###

* The current build has been tested on MacOS using XCode only. But it should work on Windows using Visual Studio 2013 with minimal effort.
* Get a recent version of CMake.
* Point CMake to 'users/marcel/avgraph'. Select an output folder to generate the project files.
* Open the project file in XCode or Visual Studio.
* To run AV Graph, make sure to build 'avgraph' in your environment and make it your run target.
* The working directory MUST be set to 'users/marcel/avgraph/data', otherwise it will not find the assets used by AV Graph, and you'll end up with a mostly empty screen due to missing fonts and images.

### Contacting the author ###

* For questions or suggestions, e-mail me at marcel303 [at] gmail.com or reach out to me on Facebook.