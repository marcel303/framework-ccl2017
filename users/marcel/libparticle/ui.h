/*
	Copyright (C) 2017 Marcel Smit
	marcel303@gmail.com
	https://www.facebook.com/marcel.smit981

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#include <string>

class Color;
class ColorWheel;
struct ParticleColor;
struct ParticleColorCurve;
struct ParticleCurve;
struct UiElem;
struct UiMenu;
struct UiMenuStates;

struct UiState
{
	int x;
	int y;
	int sx;
	
	std::string font;
	float textBoxTextOffset;
	
	float opacity;
	
	UiElem * activeElem;
	ParticleColor * activeColor;
	ColorWheel * colorWheel;
	UiMenuStates * menuStates;
	
	UiState();
	~UiState();
	
	void reset();
};

extern UiState * g_uiState;

extern bool g_doActions;
extern bool g_doDraw;
extern int g_drawX;
extern int g_drawY;

void initUi();
void shutUi();

void drawUiRectCheckered(float x1, float y1, float x2, float y2, float scale);
void drawUiCircle(const float x, const float y, const float radius, const float r, const float g, const float b, const float a);
void drawUiShadedTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color & c1, const Color & c2, const Color & c3);

extern void hlsToRGB(float hue, float lum, float sat, float & r, float & g, float & b);
extern void rgbToHSL(float r, float g, float b, float & hue, float & lum, float & sat);
extern void srgbToLinear(float r, float g, float b, float & out_r, float & out_g, float & out_b);
extern void linearToSrgb(float r, float g, float b, float & out_r, float & out_g, float & out_b);

//

void makeActive(UiState * state, const bool doActions, const bool doDraw);

void pushMenu(const char * name, const int width = 0);
void popMenu();

//

bool doButton(const char * name, const float xOffset, const float xScale, const bool lineBreak);
bool doButton(const char * name);

void doTextBox(int & value, const char * name, const float xOffset, const float xScale, const bool lineBreak, const float dt);
void doTextBox(float & value, const char * name, const float xOffset, const float xScale, const bool lineBreak, const float dt);
void doTextBox(std::string & value, const char * name, const float xOffset, const float xScale, const bool lineBreak, const float dt);

void doTextBox(int & value, const char * name, const float dt);
void doTextBox(float & value, const char * name, const float dt);
void doTextBox(std::string & value, const char * name, const float dt);

bool doCheckBox(bool & value, const char * name, const bool isCollapsable);

bool doDrawer(bool & value, const char * name);

void doLabel(const char * text, const float xAlign);
void doBreak();

struct EnumValue
{
	EnumValue()
	{
	}

	EnumValue(int aValue, std::string aName)
		: value(aValue)
		, name(aName)
	{
	}

	int value;
	std::string name;
};

void doEnumImpl(int & value, const char * name, const std::vector<EnumValue> & enumValues);

template <typename E>
void doEnum(E & value, const char * name, const std::vector<EnumValue> & enumValues)
{
	int valueInt = int(value);
	doEnumImpl(valueInt, name, enumValues);
	value = E(valueInt);
}

void doDropdownImpl(int & value, const char * name, const std::vector<EnumValue> & enumValues);

template <typename E>
void doDropdown(E & value, const char * name, const std::vector<EnumValue> & enumValues)
{
	int valueInt = int(value);
	doDropdownImpl(valueInt, name, enumValues);
	value = E(valueInt);
}

void doParticleCurve(ParticleCurve & curve, const char * name);
void doParticleColor(ParticleColor & color, const char * name);
void doParticleColorCurve(ParticleColorCurve & curve, const char * name);

void doColorWheel(float & r, float & g, float & b, float & a, const char * name, const float dt);
void doColorWheel(ParticleColor & color, const char * name, const float dt);
