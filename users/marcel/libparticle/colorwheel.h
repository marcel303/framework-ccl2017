/*
	Copyright (C) 2017 Marcel Smit
	marcel303@gmail.com
	https://www.facebook.com/marcel.smit981

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

class ColorWheel
{
	static void getTriangleAngles(float a[3]);
	static void getTriangleCoords(float xy[6]);

public:
	static const int size = 100;
	static const int wheelThickness = 20;
	static const int barHeight = 16;
	static const int barX1 = -size;
	static const int barY1 = +size + 4;
	static const int barX2 = +size;
	static const int barY2 = +size + 4 + barHeight;

	float hue;
	float baryWhite;
	float baryBlack;
	float opacity;

	enum State
	{
		kState_Idle,
		kState_SelectHue,
		kState_SelectSatValue,
		kState_SelectOpacity
	};

	State state;

	ColorWheel();

	void tick(const float mouseX, const float mouseY, const bool mouseWentDown, const bool mouseIsDown, const float dt);
	void draw();

	int getSx() const { return size * 2; }
	int getSy() const { return size * 2 + 16; }

	bool intersectWheel(const float x, const float y, float & hue) const;
	bool intersectTriangle(const float x, const float y, float & saturation, float & value) const;
	bool intersectBar(const float x, const float y, float & t) const;

	void fromColor(const float r, const float g, const float b, const float a);
	void toColor(const float hue, const float saturation, const float value, float & r, float & g, float & b, float & a) const;
	void toColor(float & r, float & g, float & b, float & a) const;
};
